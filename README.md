# TBM Voice UI

![UI](https://forum.cfx.re/uploads/default/optimized/4X/1/5/0/150e3e3a65be3ed0267c75d0db78b5ca160501d8_2_690x291.jpeg "UI View")



Simple voice range hud for salty. The speaker changes to show you what voice distance you're speaking. Inspired by Voice Range UI

## Help

if you need help. Join Discord: https://discord.gg/jgtQUKj7Tt

# Features:

* Configurable
* Speaker hud with distance
* Circle around you showing how far your voice can reach when changing the range.
* FREE

# My other Mods


* [TBM Ped Race](https://forum.cfx.re/t/tbm-ped-race-like-a-horse-race/4935189)
* [TBM Authority Net (Medic)](https://forum.cfx.re/t/tbm-authority-net-for-medic/4946999)
* [TBM Authority Net (Police)](https://forum.cfx.re/t/tbm-authority-net-police/4895485)
* [TBM Multi Player/Location and Vehicle Robbery for ESX and QBCore](https://forum.cfx.re/t/tbm-multi-player-location-and-vehicle-robbery-for-esx-and-qbcore/4817636)
* [TBM Destruction Derby](https://forum.cfx.re/t/tbm-destruction-derby/4789138)
* [TBM Racing](https://forum.cfx.re/t/esx-paid-tbm-racing/4729734)
* [TBM Public Garage and Parking System](https://forum.cfx.re/t/tbm-public-garage-and-parking-system/4784269)
* [TBM Dynamic Dealer](https://forum.cfx.re/t/paid-esx-dynamic-dealer/4374166)
* [TBM In Game Whitelist](https://forum.cfx.re/t/paid-ingamewhitelist/4373808)
* [TBM Player Storage](https://forum.cfx.re/t/paid-esx-tbm-player-storage/4762262)
* [TBM Voice UI](https://forum.cfx.re/t/tbm-voice-ui/4774657)
* [TBM Player Garage](https://forum.cfx.re/t/esx-tbm-player-garage/4778785)
* [TBM Job Change after death](https://forum.cfx.re/t/tbm-job-change-after-death/4811280)
* [TBM Register Exchange Items](https://forum.cfx.re/t/tbm-register-exchange-items/4828934)
* [TBM Vehicle Deleter](https://forum.cfx.re/t/tbm-vehicle-deleter/4852630)
* [TBM Peds Blips Marker Object Creator](https://forum.cfx.re/t/tbm-peds-blips-marker-object-creator/4852644)
* [TBM Prevent Double Connections](https://forum.cfx.re/t/tbm-prevent-double-connections/4863062)
* [TBM Player Vehicle Market for QB Core/ESX](https://forum.cfx.re/t/tbm-player-vehicle-market-for-qb-core-an-esx/4900718)

# My Shop

* [https://tb-mods.com/](https://tb-mods.com/)



### License

You are not allowed to use this mod without a licence. You are not allowed to share the code or republish it in any way. 
